package com.ipp.springcloud.appstorage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ipp.springcloud.appstorage.entity.Storage;
import com.ipp.springcloud.appstorage.mapper.StorageMapper;
import com.ipp.springcloud.appstorage.service.IStorageService;
import org.springframework.stereotype.Service;

@Service
public class StorageServiceImpl extends ServiceImpl<StorageMapper, Storage> implements IStorageService {
    /**
     * 减库存
     *
     * @param commodityCode
     * @param count
     */
    @Override
    public void deduct(String commodityCode, int count) {
        QueryWrapper<Storage> wrapper = new QueryWrapper<>();
        wrapper.setEntity(new Storage().setCommodityCode(commodityCode));
        Storage storage = baseMapper.selectOne(wrapper);
        storage.setCount(storage.getCount() - count);
        baseMapper.updateById(storage);
    }
}