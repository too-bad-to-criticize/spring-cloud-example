package com.ipp.springcloud.appstorage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ipp.springcloud.appstorage.entity.Storage;

public interface StorageMapper extends BaseMapper<Storage> {
}
