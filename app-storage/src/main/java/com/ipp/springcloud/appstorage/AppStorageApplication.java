package com.ipp.springcloud.appstorage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient  // 开启eureka客户端模式
@MapperScan("com.ipp.springcloud.appstorage.mapper") // mybatis扫描
public class AppStorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppStorageApplication.class, args);
    }

}
