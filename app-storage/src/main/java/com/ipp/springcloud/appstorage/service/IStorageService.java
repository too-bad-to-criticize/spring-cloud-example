package com.ipp.springcloud.appstorage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ipp.springcloud.appstorage.entity.Storage;

public interface IStorageService extends IService<Storage> {
    void deduct(String commodityCode, int count);
}