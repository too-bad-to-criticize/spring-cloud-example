package com.ipp.springcloud.apporder.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 库存服务提供的接口
 * @author david
 */
@FeignClient(name = "APP-STORAGE")
public interface StorageFeignService {

    /**
     * 减库存
     * @param commodityCode
     * @param count
     * @return
     */
    @GetMapping("/storage/deduct")
    Boolean deduct(@RequestParam("commodityCode") String commodityCode, @RequestParam("count") Integer count);

}
