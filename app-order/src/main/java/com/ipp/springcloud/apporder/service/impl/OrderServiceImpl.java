package com.ipp.springcloud.apporder.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ipp.springcloud.apporder.entity.Order;
import com.ipp.springcloud.apporder.feign.StorageFeignService;
import com.ipp.springcloud.apporder.mapper.OrderMapper;
import com.ipp.springcloud.apporder.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author david
 * @since 2020-03-16
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Autowired
    private StorageFeignService storageFeignService;


    /**
     * 下单：创建订单、减库存，涉及到两个服务
     *
     * @param userId
     * @param commodityCode
     * @param count
     */
    @Override
    public void placeOrder(String userId, String commodityCode, Integer count) {
        BigDecimal orderMoney = new BigDecimal(count).multiply(new BigDecimal(5));
        Order order = new Order()
                .setUserId(userId)
                .setCommodityCode(commodityCode)
                .setCount(count)
                .setMoney(orderMoney);
        baseMapper.insert(order);
        storageFeignService.deduct(commodityCode, count);

    }

}
