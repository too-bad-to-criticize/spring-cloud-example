package com.ipp.springcloud.apporder.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author david
 * @since 2020-03-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_order")
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type= IdType.AUTO)
    @TableField("id")
    private Integer id;

    @TableField("user_id")
    private String userId;
    public static final String TBL_USER_ID = "user_id";

    @TableField("commodity_code")
    private String commodityCode;
    public static final String TBL_COMMODITY_CODE = "commodity_code";

    @TableField("count")
    private Integer count;
    public static final String TBL_COUNT = "count";

    @TableField("money")
    private BigDecimal money;
    public static final String TBL_MONEY = "money";
}