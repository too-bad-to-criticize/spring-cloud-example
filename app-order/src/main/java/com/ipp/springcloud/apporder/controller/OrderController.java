package com.ipp.springcloud.apporder.controller;


import com.ipp.springcloud.apporder.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author david
 * @since 2020-03-16
 */
@RestController
@RequestMapping("/order")
public class OrderController {


    @Autowired
    private IOrderService orderServiceImpl;
    /**
     * 下单：插入订单表、扣减库存，模拟回滚
     *
     * @return
     */
    @RequestMapping("/placeOrder/commit")
    public Boolean placeOrderCommit() {

        orderServiceImpl.placeOrder("1", "product-1", 1);
        return true;
    }

    /**
     * 下单：插入订单表、扣减库存，模拟回滚
     *
     * @return
     */
    @RequestMapping("/placeOrder/rollback")
    public Boolean placeOrderRollback() {
        // product-2 扣库存时模拟了一个业务异常,
        orderServiceImpl.placeOrder("1", "product-2", 1);
        return true;
    }

}
