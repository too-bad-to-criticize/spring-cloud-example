package com.ipp.springcloud.apporder.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ipp.springcloud.apporder.entity.Order;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author david
 * @since 2020-03-16
 */
public interface IOrderService extends IService<Order> {
    void placeOrder(String userId, String commodityCode, Integer count);
}
