package com.ipp.springcloud.apporder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ipp.springcloud.apporder.entity.Order;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author david
 * @since 2020-03-16
 */
public interface OrderMapper extends BaseMapper<Order> {

}
