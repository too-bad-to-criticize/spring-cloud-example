package com.ipp.springcloud.appauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient  // 开启eureka客户端模式
public class AppAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppAuthApplication.class, args);
    }

}
