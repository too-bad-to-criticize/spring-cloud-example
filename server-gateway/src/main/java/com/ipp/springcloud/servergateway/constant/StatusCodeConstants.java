package com.ipp.springcloud.servergateway.constant;

public interface StatusCodeConstants {

    String TOKEN_NONE = "E0001";
    String TOKEN_ERROR = "E0002";
    String FREQUENT_REQUESTS = "E0003";

}
